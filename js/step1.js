const step1 = () => {
    /**
     * Инициализация переменных
     */
    window._distance = 0;
        // Подсказки
    let suggestView,
        // Точки на карте
        points = [],
        // Объект карты
        myMap = null,
        // Счётчик точек маршрута
        countAddresses = 2,
        // Адреса
        addresses = [],
        // Шаблон input'а
        template = `
            <div class="group">
                <input type="text" id="address-%idAddr%" size="50" value="%value%" required>
                <label>%label%</label>
                %del%
            </div>
          `,
        // Метки для input'ов адресов
        labels = [
            'Откуда',
            'Куда',
            'Куда ещё',
            'Конечный адрес'
        ];

    // Маршрут по умолчанию
    const defaultAddrs = ["Москва", "Тверь"];

    ymaps.ready(init);

    function init() {
        $(document).ready(function () {
            setListeners();
        })
    }

    /**
     * Настройка параметров карты
     */
    function setupMap(defaultCoord = []) {

        // Обнуляем переменную и удаляем тэги Я.Карт
        myMap = null;
        $("ymaps").remove();

        // Заполняем точки из данных формы
        let center = (points.length < 1) ? [56.399625, 36.71120] : defaultCoord;


        myMap = new ymaps.Map('map', {
            center: center,
            zoom: 7,
        }, {
            buttonMaxWidth: 300
        });

        /**
         * Обработчик двойного клика
         * для добавления точек на карту, если в инпутах
         * заполнено меньше 2 полей адреса
         *
         */
        let handler = myMap.events.group();

        if (points.length < 2) {

            if (points.length === 1) {
                setGeoObject(defaultCoord);
            }

            handler.add('dblclick', function (e) {
                e.preventDefault();
                let coords = e.get('coords');

                setGeoObject(coords);
                points.push(coords);
                if (points.length > 1) {
                    setupRouter(points);
                    myMap.geoObjects.removeAll();
                    handler.removeAll();
                }
            });
        }
        else
            setupRouter(points);

    }

    /**
     * Настройка маршрутизации
     */
    function setupRouter(referencePoints) {



        // console.log("points = " + points);

        ymaps.route(
            referencePoints,
            {
                mapStateAutoApply: true
            })

            .then(function (route) {

                fillAddresses(route);

                myMap.geoObjects.add(route);

                if (points.length > 1)
                    setDistance(route.getLength());

                // При любом изменении маршрута заполняем массив адресов и
                // заполняем тэг с классом distance
                route.editor.events.add(["start", "routeupdate"], function (e) {
                    fillAddresses(route);
                    setDistance(route.getLength());
                    countAddresses = route.getWayPoints().getLength();

                });

                // Включаем режим редактирования только на удаление точек
                route.editor.start({addWayPoints: false, removeWayPoints: true});

                /**
                 * Обработка событий карты и редактора
                 */

                // При клике на любое место на карте включается режим редактирования с возможностью добавлять точки
                myMap.events.add('click', function (e) {
                    if (route.getWayPoints().getLength() < 4) {
                        route.editor.start({addWayPoints: true, removeWayPoints: true});
                        countAddresses = route.getWayPoints().getLength();
                    }
                });

                // После того как точка добавлена ил удалена - отключаем режим добавления точек
                route.editor.events.add(["waypointadd", "waypointremove"], function () {
                    route.editor.start({
                        addWayPoints: false,
                        removeWayPoints: true,
                        editViaPoints: false,
                        addViaPoints: false
                    });
                    countAddresses = route.getWayPoints().getLength();
                });

                // route.editor.events.add(["routeupdate"], function () {
                //
                // });


            }, function (error) {
                alert("Возникла ошибка: " + error.message);
            });
    }

    /**
     * Заполняем массив точек данными из input'ов адрессов
     *
     * @returns {Array}
     */
    function fillArr() {
        points = [];
        $(".addreses :input").each(function (key, value) {
            if ($(this).val() != "") {
                points[key] = $(this).val();
            }
        });
        return points;
    }

    function setGeoObject(coords) {
        let myGeoObject = new ymaps.GeoObject({
            geometry: {
                type: "Point", // тип геометрии - точка
                coordinates: coords // координаты точки
            }
        });
        myMap.geoObjects.add(myGeoObject);
        // points.push(coords);
    }

    /**
     * Заполняем массив адресов
     *
     * @param route
     * @returns {Promise<void>}
     */
    async function fillAddresses(route) {
        points = route.requestPoints;

        for (let key in points) {
            let myReverseGeocoder = ymaps.geocode(points[key]);
            await myReverseGeocoder.then(function (res) {
                addresses[key] = res.geoObjects.get(0).properties.get('text');
                fillInputs();
            });
        }
    }

    function fillInputs() {
        $(".addreses").html(function () {
            // let output = '<div class="addreses">';
            let output = '';

            $(points).each(function (index, value) {
                let address = '';
                if (typeof(addresses[index]) === 'undefined')
                    addresses[index] = '';

                let input = template.replace(/%idAddr%/, index)
                    .replace(/%label%/, labels[index])
                    .replace(/%value%/, addresses[index]);
                if (index > 1)
                    input = input.replace(/%del%/, `<span>Удалить</span>`);
                else
                    input = input.replace(/%del%/, '');

                output += input;
            });

            return output;
        });

        let event = new Event("change");
        formcalc.dispatchEvent(event);
    }

    async function getCoordinates(point) {
        let myReverseGeocoder = ymaps.geocode(point);
        return await myReverseGeocoder.then(function (res) {
            let coord = res.geoObjects.get(0).geometry.getCoordinates();
            // console.log(coord);

            setupMap(coord);
        });
    }

    async function setDistance(pathLenght) {
        // console.log(pathLenght);
        window._distance = Math.floor(pathLenght / 1000);
        $(".distance").empty()
            .append(`Общее расстояние..................... ${window._distance} км.`);
    }

    /**
     * Регистрируем слушателей событий элементов dom
     */
    function setListeners() {

        /**
         * При фокусе на input адресов включаем подсказки
         */
        $('.addreses').on('focus', 'input', function () {
            var id = $(this).attr("id");
            $(".addreses .group ymaps").remove();
            suggestView = new ymaps.SuggestView(id); // подсказки
            suggestView.events.add('select', function (event) {
                //console.log(event.get('item').value);
                points = fillArr();
                getCoordinates(points[0])
            });
        });

        $('.datetime').on('focus', 'input', function () {
            $('.timeFeed input[type=radio]').prop("checked", false);
        });

        $('.timeFeed input[type=radio]').change(function() {
            $('.datetime input').val("");
        });

        /**
         * Добавляем новое поле при клике на "Добавить адрес"
         */
        $(".addAddress").click(function () {

            if (countAddresses >= 4)
                return;
            let input = template.replace(/%idAddr%/, countAddresses)
                .replace(/%label%/, labels[countAddresses])
                .replace(/%value%/, '')
                .replace(/%del%/, `<span>Удалить</span>`);

            $(".addreses").append(input);
            countAddresses++;
        });

        /**
         * Удаляем поле при клике на "Удалить"
         */
        $('.addreses').on('click', 'span', function () {
            $(this).parent('div').remove();
            countAddresses--;
            points = fillArr();
            getCoordinates(points[0])
        });

        /**
         * Открываем модалку с картой при клике на "Показать карту"
         */
        $(document).ready(function () {
            setupMap();
        });

    }

};

export default step1;
