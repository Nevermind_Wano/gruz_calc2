const step2 = () => {
    let car_price = {
        'porter': {hour: 550, km: 13 },
        '3m': {hour: 590, km: 14 },
        '4m': {hour: 890, km: 20 },
        '5m': {hour: 990, km: 25 }
    };

    let porter_price = 330;

    let cost = 0;

    $('form.calc').on("change", function () {
        console.log('change');
        let chosen_car = $('input[name=car]:checked', 'form.calc').val();
        let count_porters = parseInt($('input[name=count_gruz]', 'form.calc').val());
        let lease_time = parseInt($('input[name=time_arenda]', 'form.calc').val());


        let time_cost = car_price[chosen_car].hour * lease_time;
        let distance_cost = car_price[chosen_car].km * window._distance;
        let porter_cost = count_porters * porter_price * lease_time;
        let cost = time_cost + distance_cost + porter_cost;


        $(".cost").empty()
            .append(`Стоимость............................ ${cost} руб.`);

    });


    $("#client_tel").mask("+7(999) 999-99-99");


};


export default step2;