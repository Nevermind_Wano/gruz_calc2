var webpack = require('webpack');
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

const path = require('path');
const WebpackAssetsManifest = require('webpack-assets-manifest');

module.exports = {
    context: __dirname,
    // devtool: "source-map",
    entry: "./js/index.js",
    output: {
        path: path.join( __dirname, 'dist' ),
        filename: "calc1.js",
        chunkFilename: '[id]-[chunkhash].js',
    },
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                cache: true,
                parallel: true,
                sourceMap: true // set to true if you want JS source maps
            }),
            new OptimizeCSSAssetsPlugin({})
        ]
    },
    plugins: [
        new WebpackAssetsManifest({
            // Options go here
        }),
        new MiniCssExtractPlugin({
            filename: "calc_styles.css"
//  	      filename: "styles.css"
        })
    ],
    module: {
        rules: [
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: [
                        // fallback to style-loader in development
                        MiniCssExtractPlugin.loader,
                        "css-loader",
                        "sass-loader"
                    ]
            },
            {
                test: /fonts\/.*\.(woff|woff2|eot|ttf|svg)$/,
                loader: 'file-loader?name=[name].[ext]',
            },
            {
                test: /img\/.*\.(jpg|png)$/,
                loader: 'file-loader?name=[name].[ext]',
            },
        ]
    },
    // devServer: {
    //   contentBase: path.join(__dirname, "dist"),
    //   compress: true,
    //   port: 9000
    // }
}
